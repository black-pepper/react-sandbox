import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import TabContainer from './TabContainer';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<TabContainer />, document.getElementById('root'));
registerServiceWorker();
