import React from "react";

const SubtabBody = ({tabs, currentStab}) => (
  <div>
    {tabs.map((stab, i) =>
      currentStab === i && (
        <div>
          Sous tab {i} !
        </div>
      ))}
  </div>
)

export default SubtabBody;
