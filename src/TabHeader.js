import React from "react";

const TabHeader = ({ tabs, changeTab }) => (
  <div style={header}>
    {tabs.map((tab, i) => (
      <a key={i} href={"#"} onClick={e => changeTab(e, i)} style={tabLink}>
        {tab.name}
      </a>
    ))}
  </div>
);

const header = {
  display: "flex",
  justifyContent: "space-around",
  lineHeight: "34px"
};

const tabLink = {
  backgroundColor: "#dedede",
  flex: 1,
  textAlign: "center"
};

export default TabHeader;
