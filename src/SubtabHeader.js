import React from "react";

const SubtabHeader = ({ tabs, changeTab }) => (
  <div style={header}>
    {tabs.map((tab, i) => (
      <a key={i} href={"#"} onClick={e => changeTab(e, i)} style={tabLink}>
        {tab.name}
      </a>
    ))}
  </div>
);

const header = {
  display: "flex",
  flexFlow: 'column nowrap',
  justifyContent: "space-around",
  lineHeight: "34px"
};

const tabLink = {
  backgroundColor: "#dedede",
  flex: 1,
  textAlign: "center"
};

export default SubtabHeader;
